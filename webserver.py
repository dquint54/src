from flask import Flask, render_template, request
import requests
import os
app = Flask(__name__,template_folder='templates')
app.config['UPLOAD_FOLDER'] = "/home/pi/CpE185_Final_Project/src/static" 

@app.route('/temperature', methods=['POST']) 
def temperature():
    zipcode = request.form['zip']
    r=requests.get('http://api.openweathermap.org/data/2.5/weather?zip='+zipcode+',us&appid=c27898dfbb483ed2871da02dede1d47e') 
    json_object = r.json()
    temp_k = float(json_object['main']['temp'])
    temp_f = (temp_k - 273.15) * 1.8 + 32
    return render_template('index.html', temp=temp_f)

@app.route('/')
def index():
	full_filename = os.path.join(app.config['UPLOAD_FOLDER'], 'thermister_graph.png')

	return render_template('index.html', user_image = full_filename)

if __name__  == '__main__':
    app.run(debug=True,host='0.0.0.0' , port='5000')
