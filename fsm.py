from gpiozero import LED
from time import sleep
import csv
import numpy as np
import serial
import re

#initialize serial port
ser = serial.Serial()
ser.port = '/dev/ttyACM0' #STM32 serial port
ser.baudrate = 38400
ser.timeout = 10 #specify timeout when using readline()
ser.open()

ledGreen = LED(2)
ledYellow = LED(3)
ledRed = LED(4)

def average_temp():
	with open("thermister_data.csv", 'r') as f:
		list = [row.split(',')[0] for row in f]
	list = np.array(list, dtype=np.float32)	
	avg = np.average(list)
	return avg

while True:
	# avg = average_temp()	# Optionally use averaged csv file as input
	line=ser.readline()
	line = re.sub("[^0-9]","",str(line))
	if int(float(line)) < 2000:
		line=ser.readline()
	line = re.sub("[^0-9]","",str(line))
	avg = float(int(line))/100.0
	print(avg)
	if (avg > 24.66) and (avg < 24.69):
		ledYellow.off()
		ledRed.off()
		ledGreen.on()
	elif (avg <= 24.66 and avg >= 24.63) or (avg >= 24.69 and avg <= 24.71):
		ledYellow.on()
		ledRed.off()
		ledGreen.off()
	elif (avg > 24.71) or (avg < 24.63):
		ledYellow.off()
		ledRed.on()
		ledGreen.off()
	else:
		ledYellow.off()
		ledRed.on()
		ledGreen.off()
	sleep(1)