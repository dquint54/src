import matplotlib.pyplot as plt
import serial
from datetime import datetime
import re
import time
import csv

#initialize serial port
ser = serial.Serial()
ser.port = '/dev/ttyACM0' #STM32 serial port
ser.baudrate = 38400
ser.timeout = 10 #specify timeout when using readline()
ser.open()
# if ser.is_open==True:	#Debug
	# print("\nSerial port now open. Configuration:\n")
	# print(ser, "\n") #print serial parameters

# Generate a csv file name thermister_data.csv containing 60 seconds of thermister input
def generate_csv():
	csv = []
	for x in range(1,10):
		line=ser.readline()
		line = re.sub("[^0-9]","",str(line))
		if int(line) < 2000:
			line=ser.readline()
		line = re.sub("[^0-9]","",str(line))
		temp_celsius = float(int(line))/100.0
		now = datetime.now()
		current_seconds = ',' + str(now.strftime("%M:%S"))
		data = str(temp_celsius) + str(current_seconds)
		csv.append(data)
		time.sleep(6)
	for i in csv:
		#print(i)	#Debug
		with open('thermister_data.csv', 'w') as f:
			for item in csv:
				f.write("%s\n" % item)

# Generate thermister_graph.png plot based on generated values from csv file				
def generate_plot():
	x=[]
	y=[]
	
	with open('thermister_data.csv', 'r') as csvfile:
		plots = csv.reader(csvfile, delimiter=',')
		for row in plots:
			x.append(str(row[1]))
			y.append(float(row[0]))    
	plt.plot(x,y, marker='o')
	plt.title("Analog to Digital Thermister Serial Data Across 60 Seconds Every 5 Minutes")
	plt.xlabel('Time(s)')
	plt.ylabel('Temperature(C)')
	plt.savefig('/home/pi/CpE185_Final_Project/src/static/thermister_graph.png')	

generate_csv()
generate_plot()
ser.close()
